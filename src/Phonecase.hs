{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Phonecase (flexPart, solidPart, ring, buttons) where

import qualified Csg

phoneW = 75.5
phoneD = 155
phoneH = 8
phoneR = 10
wallT = 2

roundedRect :: Double -> Double -> Double -> Csg.BspTree
roundedRect width depth radius = Csg.unionConcat $ p1 : p2 : cylinders
  where
    p1 = Csg.scale (width - 2*radius, depth, 1) Csg.unitCube
    p2 = Csg.scale (width, depth-2*radius, 1) Csg.unitCube
    cylinder = Csg.scale (radius, radius, 1) $ Csg.unitCylinder 32
    halfW = width/2 -radius
    halfD = depth/2 - radius
    cylinders =  [ Csg.translate (x, y, 0) cylinder | x <- [-halfW, halfW], y <- [-halfD, halfD]]

oblong :: Double -> Double -> Csg.BspTree
oblong width depth = Csg.unionConcat $ center : cylinders
  where
    center = Csg.scale (width, depth-width, 1) Csg.unitCube
    cylinder = Csg.scale (width/2, width/2, 1) $ Csg.unitCylinder 32
    cylinders = [Csg.translate (0, y, 0) cylinder | y <- [-(depth-width)/2, (depth-width)/2]]

flexPart = phoneExterior `Csg.subtract` (Csg.unionConcat $ solidPart:phoneInterior:phoneScreenHole:holes)
  where
    phoneExterior = Csg.scale (1, 1, phoneH + wallT*2) $ 
                      roundedRect 
                        (phoneW + wallT*2) (phoneD + wallT*2) (phoneR + wallT)
    phoneInterior = Csg.scale (1, 1, phoneH) $
                        roundedRect phoneW phoneD phoneR
    phoneScreenHole = Csg.translate (0, 0, (phoneH/2+wallT)) $ 
                        Csg.scale (1, 1, phoneH) $ 
                          roundedRect (phoneW-wallT*2) (phoneD-wallT*2) phoneR
    cameraHole = Csg.translate (phoneW/2 - 18, phoneD/2-9, -phoneH/2) $ 
                   Csg.scale (1, 1, 10) $
                    Csg.rotate (0, 0, 1) (pi/2) $
                     oblong 9 28
    fingerHole = Csg.translate (0, phoneD/2 - 36, -phoneH/2) $
                    Csg.uniformScale 8 $ Csg.unitCylinder 32

    irUsbHole = Csg.rotate (1, 0, 0) (pi/2) $
                  Csg.scale (1, 1, phoneD*2) $ 
                    Csg.rotate (0, 0, 1) (pi/2) $
                      oblong 5 12
    headphoneHole = Csg.translate (-22, -phoneD/2, 0) $
                    Csg.rotate (1, 0, 0) (pi/2) $
                      Csg.scale (3, 3, 10) $ Csg.unitCylinder 16
    buttonHolesInner = Csg.translate (phoneW/2, phoneD/2 - 30 - 38/2, 0) $
                        Csg.rotate (0, 1, 0) (pi/2) $
                         Csg.scale (1, 1, 1.75) $ oblong (2 + 0.5) (38+1)

    buttonHolesPower = Csg.translate (phoneW/2+wallT, phoneD/2 - 57 - 10/2, 0) $
                        Csg.rotate (0, 1, 0) (pi/2) $
                          oblong 2 10

    buttonHolesVolume = Csg.translate (phoneW/2+wallT, phoneD/2 - 30 - 21/2, 0) $
                        Csg.rotate (0, 1, 0) (pi/2) $
                          oblong 2 21
    holes = [
            cameraHole, 
            fingerHole, 
            irUsbHole, 
            headphoneHole,
             buttonHolesInner, buttonHolesPower, buttonHolesVolume
        ] 

hexGrid = Csg.unionConcat [Csg.translate (0, i*(5*(sqrt 3)+1), 0) twoRows | i <- [0..16]]
  where
    hex = Csg.scale (5, 5, 1) $ Csg.unitCylinder 6
    row = Csg.unionConcat [Csg.translate (i*(10+5+(sqrt 3)), 0, 0) hex | i <- [0..5]]
    twoRows = row `Csg.union` (Csg.translate (5 + 2.5 + 0.5*(sqrt 3), 2.5*(sqrt 3) + 0.5, 0) row)

center :: Csg.BspTree -> Csg.BspTree
center obj = Csg.translate middle obj
  where
    ((x1, y1, z1),(x2,y2, z2)) = Csg.aabb obj
    middle = (-(x1+x2)/2, -(y1+y2)/2, -(z1+z2)/2)


ring = base `Csg.subtract` baseInner 
  where 
    base = roundedRect (phoneW + wallT*2) (phoneD + wallT*2) (phoneR + wallT)
    baseInner = Csg.scale (1, 1, 2) $
        roundedRect (phoneW + wallT*2-2) (phoneD + wallT*2-2) (phoneR + wallT-1)

buttons = (oblong 2 10) `Csg.union` ( Csg.translate (10 , 10, 0) $ oblong 2 21)

solidPart = Csg.unionConcat [
                Csg.translate (0, 0, -(phoneH/2 + wallT -0.5)) bottom, 
                Csg.translate (0, 0, (phoneH/2 +wallT -0.5)) rim,
                tbPillars, lrPillars
            ]
  where
    base = roundedRect (phoneW + wallT*2) (phoneD + wallT*2) (phoneR + wallT)
    baseInner = Csg.scale (1, 1, 2) $
        roundedRect (phoneW + wallT*2-2) (phoneD + wallT*2-2) (phoneR + wallT-1)

    cameraHole = Csg.translate (phoneW/2 - 18, phoneD/2-9, 0) $ 
                   Csg.scale (1, 1, 10) $
                    Csg.rotate (0, 0, 1) (pi/2) $
                     oblong 9 28

    cameraRim = (Csg.translate (phoneW/2 - 18, phoneD/2-9, 0) $ 
                  Csg.rotate (0, 0, 1) (pi/2) $
                    oblong (9+2) (28+2)) `Csg.subtract` cameraHole

    fingerHole = Csg.translate (0, phoneD/2 - 36, 0) $
                    Csg.uniformScale (8) $ Csg.unitCylinder 32

    fingerRim = (Csg.translate (0, phoneD/2 - 36, 0) $
                  Csg.scale (8+1, 8+1, 1) $ Csg.unitCylinder 32) `Csg.subtract` fingerHole

    rim = base `Csg.subtract` baseInner
   
    bottom = Csg.unionConcat [rim, cameraRim, fingerRim, 
                  base `Csg.subtract` 
                  (Csg.unionConcat [
                    (center hexGrid), cameraHole, fingerHole] 
                  )]
    tbPillars = Csg.unionConcat [
           Csg.translate (i*(phoneW/2-phoneR-1) , j*(phoneD/2 + wallT -0.5), 0) $
            Csg.scale (2, 1, phoneH+ wallT*2) $ 
             Csg.unitCube | i <- [-1, 1], j<- [-1, 1]]

    lrPillars = Csg.unionConcat [
           Csg.translate (i*(phoneW/2+wallT-0.5), j*(phoneD/2-phoneR-1), 0) $
            Csg.scale (1, 2, phoneH+ wallT*2) $ 
             Csg.unitCube | i <- [-1, 1], j<- [-1, 0, 1]]

