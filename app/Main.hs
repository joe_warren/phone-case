{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import qualified Csg
import qualified Csg.STL
import qualified Data.Text.IO as T

import qualified Phonecase

writeObject :: Csg.BspTree -> String -> IO ()
writeObject obj filename = do
    putStrLn $ "writing " ++ filename
    T.writeFile filename $ Csg.STL.toSTL obj
    putStrLn $ "written " ++ filename

main :: IO [()]
main = sequence $ (uncurry writeObject) <$> [
        (Phonecase.buttons, "buttons.stl"),
        (Csg.scale (10, 2, 1.0) Csg.unitCube, "cube.stl"),
        (Phonecase.ring, "ring.stl"),
        (Phonecase.solidPart, "border.stl"),
        (Phonecase.flexPart, "case.stl")
    ]
